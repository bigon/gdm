# translation of gdm2.HEAD.po to Mongolian
# translation of gdm2.gnome-2-4.po to Mongolian
# translation of gdm2.gnome-2-4.mn.po to Mongolian
# translation of gdm2.HEAD.mn.po to Mongolian
# This file is distributed under the same license as the PACKAGE package.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER.
# Ochirbat Batzaya <buuvei@yahoo.com>, 2003.
# Sanlig Badral <badral@chinggis.com>, 2003.
# Sanlig Badral <badral@openmn.org>, 2003.
# Sanlig Badral <badral@users.sf.net>, 2003.
# Sanlig Badral <Badral@openmn.org>, 2004.
#
msgid ""
msgstr ""
"Project-Id-Version: gdm2.HEAD\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2011-04-28 22:48+0200\n"
"PO-Revision-Date: 2004-03-06 23:01+0800\n"
"Last-Translator: Dulmandakh Sukhbaatar <sdulmandakh@yahoo.com>\n"
"Language-Team: Mongolian <openmn-core@lists.sf.net>\n"
"Language: mn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../default.desktop.in.h:1
msgid "System Default"
msgstr "Системийн стандарт"

#: ../default.desktop.in.h:2
msgid "The default session on the system"
msgstr ""

#: ../patches/11_xephyr_nested.patch:747
msgid "Run gdm in a nested window"
msgstr ""

#: ../patches/11_xephyr_nested.patch:748
msgid "Don't lock the screen on the current display"
msgstr ""

#: ../patches/11_xephyr_nested.patch:1052
msgid "Disconnect"
msgstr "Салгах"

#: ../patches/11_xephyr_nested.patch:1056
msgid "Quit"
msgstr "Гарах"
